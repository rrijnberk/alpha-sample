import { Component } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AlphaClients';

  public profileForm = new FormGroup({
    firstName: new FormControl('Richard'),
    lastName: new FormControl('Rijnberk'),
    home: new FormGroup({
      street: new FormControl('Veersemeerstraat 37'),
      city: new FormControl('Berkel en Rodenrijs'),
      state: new FormControl('Zuid Holland'),
      zip: new FormControl('2652JL')
    }),
    work: new FormGroup({
      street: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(''),
      zip: new FormControl('')
    })
  });
}
