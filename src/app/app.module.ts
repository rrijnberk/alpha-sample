import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormAddressGroupModule } from '../components/form/address-group/address-group.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormMultiLingualModule } from '../directives/form/multi-lingual/multi-lingual.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormAddressGroupModule,
    FormMultiLingualModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
