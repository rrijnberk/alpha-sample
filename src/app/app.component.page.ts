import {GenericElementFinder} from '../../e2e/support/po/protractor.extensions';
import {by} from 'protractor';
import {AddressGroupComponentPo} from '../components/form/address-group/address-group.component.po';

export class AppComponentPage extends GenericElementFinder {

  get profileForm() {
    return new ProfileFormPO(this.element(by.css('form')));
  }
}

export class ProfileFormPO extends GenericElementFinder {
  get firstName() {
    return this.element(by.css('input[formControlName="firstName"]'));
  }

  get lastName() {
    return this.element(by.css('input[formControlName="lastName"]'));
  }

  get home() {
    return new AddressGroupComponentPo(this.element(by.css('form-address-group[label="home"]')));
  }

  get work() {
    return new AddressGroupComponentPo(this.element(by.css('form-address-group[label="work"]')));
  }
}
