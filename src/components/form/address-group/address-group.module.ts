import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {AddressGroupComponent} from './address-group.component';

@NgModule({
  declarations: [
    AddressGroupComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    AddressGroupComponent
  ]
})
export class FormAddressGroupModule {}
