import {Component, Input} from '@angular/core';
import {ControlContainer} from '@angular/forms';


@Component({
  selector: 'form-address-group',
  styleUrls: ['./address-group.component.scss'],
  templateUrl: './address-group.component.html'
})
export class AddressGroupComponent {
  @Input() label: string;

  constructor(public controlContainer: ControlContainer) {}
}
