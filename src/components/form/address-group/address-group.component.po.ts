import {GenericElementFinder} from '../../../../e2e/support/po/protractor.extensions';
import {by} from 'protractor';

export class AddressGroupComponentPo extends GenericElementFinder {
  get street() {
    return this.element(by.css('[formControlName="street"]'));
  }

  get city() {
    return this.element(by.css('[formControlName="city"]'));
  }

  get state() {
    return this.element(by.css('[formControlName="state"]'));
  }

  get zip() {
    return this.element(by.css('[formControlName="zip"]'));
  }
}
