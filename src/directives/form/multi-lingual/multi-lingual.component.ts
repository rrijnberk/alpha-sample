import {Component} from '@angular/core';

@Component({
  template: `<label>Select language</label><select><option *ngFor="let language of languages">{{language}}</option></select>`
})
export class MultiLingualComponent {
  languages = ['Dutch', 'English', 'German', 'Spanish'];
}
