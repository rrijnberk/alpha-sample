import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MultiLingualDirective} from './multi-lingual.directive';
import {MultiLingualComponent} from './multi-lingual.component';

@NgModule({
  declarations: [
    MultiLingualDirective,
    MultiLingualComponent
  ],
  imports: [
    CommonModule
  ],
  entryComponents: [
    MultiLingualComponent
  ],
  exports: [
    MultiLingualDirective
  ]
})
export class FormMultiLingualModule {}
