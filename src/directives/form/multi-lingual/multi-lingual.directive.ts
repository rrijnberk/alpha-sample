import {
  ComponentFactoryResolver,
  Directive,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import {MultiLingualComponent} from './multi-lingual.component';

@Directive({
  selector: '[multiLingual]'
})
export class MultiLingualDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this.viewContainer.createComponent(componentFactoryResolver.resolveComponentFactory(MultiLingualComponent));
    this.viewContainer.createEmbeddedView(templateRef);
  }
}
