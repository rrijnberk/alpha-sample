import {browser, by, element} from 'protractor';
import {AppComponentPage} from '../../src/app/app.component.page';

describe('demo app', () => {
  let page: AppComponentPage;

  beforeAll(() => {
    browser.get('/');
  });

  beforeEach(() => {
    const el = element(by.css('app-root'));
    page = new AppComponentPage(el);
  });

  it('is present.', () => {
    expect(page.isPresent()).toBe(true);
  });

  it('form is present.', () => { expect(page.profileForm.isPresent()).toBe(true); });

  describe('form has input for', () => {
    it('first name.', () => { expect(page.profileForm.firstName.isPresent()).toBe(true); });
    it('last name.', () => { expect(page.profileForm.lastName.isPresent()).toBe(true); });

    it('home address form.', () => { expect(page.profileForm.home.isPresent()).toBe(true); });
    it('work address form.', () => { expect(page.profileForm.work.isPresent()).toBe(true); });
  });

  it('says Richard as first name.', () => {
    expect(page.profileForm.firstName.getAttribute('value')).toEqual('Richard');
  });

  it('says Rijnberk as first name.', () => {
    expect(page.profileForm.lastName.getAttribute('value')).toEqual('Rijnberk');
  });

  describe('home address', () => {
    let address;

    beforeEach(() => {
      address = page.profileForm.home;
    });

    it('street says veersemeerstraat 37', () => {
      expect(address.street.getAttribute('value')).toEqual('Veersemeerstraat 37');
    });

    it('city says Berkel en Rodenrijs', () => {
      expect(address.city.getAttribute('value')).toEqual('Berkel en Rodenrijs');
    });

    it('state says Zuid Holland', () => {
      expect(address.state.getAttribute('value')).toEqual('Zuid Holland');
    });

    it('zip says 2652JL', () => {
      expect(address.zip.getAttribute('value')).toEqual('2652JL');
    });
  });

  describe('work address', () => {
    it('street is empty', () => {
      expect(page.profileForm.work.street.getAttribute('value')).toEqual('');
    });

    it('city is empty', () => {
      expect(page.profileForm.work.city.getAttribute('value')).toEqual('');
    });

    it('state is empty', () => {
      expect(page.profileForm.work.state.getAttribute('value')).toEqual('');
    });

    it('zip is empty', () => {
      expect(page.profileForm.work.zip.getAttribute('value')).toEqual('');
    });
  });

  // afterEach(async () => {
  //   // Assert that there are no errors emitted from the browser
  //   const logs = await browser.manage().logs().get(logging.Type.BROWSER);
  //   expect(logs).not.toContain(jasmine.objectContaining({
  //     level: logging.Level.SEVERE,
  //   } as logging.Entry));
  // });
});
