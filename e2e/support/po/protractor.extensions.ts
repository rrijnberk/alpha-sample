import {ElementArrayFinder, ElementFinder} from 'protractor';

export class GenericElementFinder extends ElementFinder {
  constructor(protected elementFinder: ElementFinder) {
    super(elementFinder.browser_, elementFinder.elementArrayFinder_);
  }
}

export class GenericElementArrayFinder extends ElementArrayFinder {
  constructor(protected elementArrayFinder: ElementArrayFinder) {
    super(elementArrayFinder.browser_, elementArrayFinder.elementArrayFinder_);
  }
}
